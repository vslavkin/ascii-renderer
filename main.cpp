#include <ncurses.h>
#include <iostream>
#include <filesystem>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

unsigned char* openImage (char* filename, int &width, int &height, int &channels);
char percent2char (int percent, int max);
char pixel2char(int r, int g, int b);


const std::string ascii_chars =
    " .'`^\",:;Il!i><~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$";

int main(int argc, char ** argv)
{
	for (int i=0; i < argc; ++i) {
		printf("Arg n%i: %s\r\n", i, argv[i]);
	}
	if (argc > 1 && !std::filesystem::exists(argv[1]))
		return 1;
	int x,y,n;
	unsigned char* data = openImage(argv[1], x, y, n);
	printf("Width: %u\r\nHeight: %u\r\nChannels: %u\r\n", x, y, n);
	// unsigned char image[y][x];
	// int pixel_index=0;
	// for (int i = 0; i < y; ++i) {
	// 	for (int j = 0; i < x; j++){
	// 		image[i][j] = []
	// 	}
	// }
	printf("%u\n", data[0]);
	initscr();			/* Start curses mode 		*/
	cbreak();				/* Line buffering disabled	*/
	keypad(stdscr, TRUE);		/* We get F1, F2 etc..		*/
	noecho();			/* Don't echo() while we do getch */
	start_color();
	int pixel_index = 0;
	for (int i = 0; i < y; ++i) {
		for (int j = 0; j < x; ++j) {
			mvprintw(i, j*2, "%c", pixel2char(data[pixel_index],data[pixel_index+1],data[pixel_index+2]));
			// mvprintw(i,j, "%i", j);
			pixel_index += n;
		}
		mvprintw(i, (x*2)+1, " %i", i);
	}
	for (int i = 0; i < (x*2)+1; ++i) {
		mvprintw(y+1, i, "%i", i%10);
	}
	while (true) {
	refresh();			/* Print it on to the real screen */
	int key;
	if ((key = getch()) == 'q'){
		endwin();			/* End curses mode		  */
		stbi_image_free(data);
		return 0;
	}
	}
}

unsigned char* openImage (char* filename, int &width, int &height, int &channels)
{
	if(!std::filesystem::exists(filename))
	{
		fprintf(stderr, "Error: Path not valid: %s\n", filename);
		exit(EXIT_FAILURE);  // Terminate the program with an error status
	}
	unsigned char *data = stbi_load(filename, &width, &height, &channels, 0);
	if(data == NULL){
		fprintf(stderr, "Error: Failed to open or load image: %s\n", filename);
		exit(EXIT_FAILURE);  // Terminate the program with an error status
	}
	return data;
}

char percent2char (int percent, int max)
{
	// std::cout << "size: " << ascii_chars.length()-1 << std::endl
	// 	  << "percent: " << (percent * (100.0/(double)max)) << std::endl
	// 	  << "Index: " << (ascii_chars.length()-1) * (percent * (100.0/(double)max)) / 100 << std::endl
	// 	  << "Caracter: " << ascii_chars[(ascii_chars.length()-1) * (percent * (100.0/(double)max)) / 100] << std::endl;
	return ascii_chars[(ascii_chars.length()-1) * (percent * (100.0/(double)max)) / 100 ];
}
char pixel2char (int r, int g, int b)
{
	return percent2char((r+g+b), (255*3));
}
